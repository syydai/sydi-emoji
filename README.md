# SyDi Emoji
SyDi Emoji (Stands for SyyDai / สือไท) is an emoji font that focusing on non-RGI (Not Recommended for General Interchange) emojis.

Design language: something similar to t**moji / **scord emoji but with a contrasting border.

🖔

![list of emoji](list.webp)

# Build
using [nanoemoji](https://github.com/googlefonts/nanoemoji)
```
nanoemoji --family "SyDi Emoji" --color_format glyf_colr_1 $(find ./svg -name 'u*.svg')
```